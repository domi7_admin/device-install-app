export const STORAGE_TOKEN_KEY = "token"
export const STORAGE_DEVICE_INFO_LIST_KEY = "deviceInfoList"
export const CODE_SUCCESS = 1

export const DATE_MASK = "yyyy-MM-dd hh:mm:ss";

export default {
	
	STORAGE_TOKEN_KEY,
	STORAGE_DEVICE_INFO_LIST_KEY,
	CODE_SUCCESS,
	DATE_MASK
}

Date.prototype.format = function (format) {
    let args = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3), //quarter

        "S": this.getMilliseconds()
    };
    if (/(y+)/.test(format)) format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let i in args) {
        let n = args[i];

        if (new RegExp("(" + i + ")").test(format)) format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
    }
    return format;
};

