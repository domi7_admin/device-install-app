const Fly = require("flyio/dist/npm/wx");
const fly = new Fly();

const app = getApp();


fly.interceptors.request.use((request) => {
	request.timeout = 5000;

	if(request.url.includes("/wechat/login")){
		delete request.headers['Authorization'];
	}else{		
		request.headers.Authorization = "Bearer " +global.token;
	}
	
	
	
})

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
	(response) => {
		
		if(response.data.code==="4005"){
			uni.reLaunch({
				url:"/pages/login/login"
			})
		}
		
		if(response.data.code==="4002"){
			uni.showToast({
				title:response.data.msg
			})
		}
		//只将请求结果的data字段返回
		return response.data
	},
	(err) => {
		uni.showToast({
			title:'网络异常'
		})
		//return Promise.resolve(err)
	}
)

export default fly;
