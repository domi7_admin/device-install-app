import Vue from 'vue'
import App from './App'

import cuCustom from './colorui/components/cu-custom.vue'

import constants from "./common/constants.js"
Vue.prototype.$consts = constants;
global.$consts = constants;

import config from './config.js'
Vue.prototype.$config = config;
global.$config = config;



import http from './http.js'





Vue.component('cu-custom',cuCustom)

Vue.config.productionTip = false


Vue.prototype.$http = http;

const app = new Vue({
    ...App
})
app.$mount()

 



