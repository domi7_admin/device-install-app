export default [{
		key: 'map',
		iconClass: 'iconditu',
		titile: '地图分布',
		param: '../map/map'
	},
	
	{
		key: 'qr',
		iconClass: 'iconsaoma',
		titile: '设备报装',
		param: '../requestInstall/requestInstall'
	},
	
	{
		key: 'list',
		iconClass: 'iconsaoma',
		titile: '报装列表',
		param: '../list/list'
	},
	
	{
		key: 'logout',
		iconClass: 'icontuichu',
		titile: '退出',
		param: '../login/login'
	}
]
